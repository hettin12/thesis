\section{Introduction}
    \label{ch2:intro}
    With the increasing number of large survey telescopes, we are entering an era of large-data astronomy \citep{decadal10}.  With a greater throughput of celestial objects observed and a wider span of re-observation cadences, a new window of observation has opened.  The accessibility of this time-domain window is growing with the recent introduction of programs such as CRTS \citep{djorgovski11}, PTF \citep{law09}, Pan-STARRS \citep{kaiser02}, and DES \citep{des05}, and with upcoming programs such as LSST \citep{lsst09}.  The increased access to the time-domain dimension has provided astronomers an additional avenue to approach research in various areas including exoplanets, variable stars, microlensing events, novae and supernovae, and active galactic nuclei.  We will also likely see the emergence of unknown phenomena that, until now,  have been unobservable.

    Before the adoption of multi-fiber spectrographs, obtaining spectra for targets in the sky was a time-consuming process.  Due to the dispersion of the light source across the CCD, longer exposure times are needed to obtain high signal-to-noise ratios (\snr) in spectra.  Larger exposure times meant that gathering valuable spectra for a population of objects would be infeasible.  Multi-fiber spectrographs introduced parallelization to the observing process.  Bundles of hundreds or thousands of optical fibers can now be positioned in the field-of-view to gather light from multiple sources simultaneously, greatly reducing the time to gather spectra for groups of objects, and increasing the efficiency of observing by orders of magnitude.  We are seeing an increase in the development and use of multi-fiber spectroscopic instruments with LAMOST \citep{cui12}, BOSS \citep{smee13}, Hectospec \citep{fabricant05}, and others, and we will continue to see development with instruments such as the upcoming PFS \citep{sugai12} and DESI \citep{levi13}.

    With the increase in the number of astronomical sources and the amount of coverage of sources at each observation, it is important to consider the methods used for data extraction.  One goal of the work of \cite{hettinger15} was to investigate the usefulness of a method for identifying radial velocity (RV) variability in spectra obtained from the multi-fiber spectrograph used in the Sloan Digital Sky Survey (SDSS; \citealp{york00}).  In this chapter we present, in detail, the methods adopted in \cite{hettinger15} during the development of a pipeline designed for obtaining radial velocity measurements from sub-exposure spectra taken with the SDSS multi-fiber spectrograph.  Section \ref{ch2:sdss} gives a brief description of the SDSS survey, discussing the sub-exposure properties inherent to typical multi-fiber spectroscopic surveys, and also gives a cautionary warning with regards to the systematic uncertainties observed in the SDSS sub-exposures.  Section \ref{ch2:radialVelocities} details the process for obtaining radial velocities from sub-exposure spectra using template cross-correlation.  We discuss empirical estimates of the uncertainties in Section \ref{ch2:errors}.  And finally, one metric for measuring variability in a source is briefly discussed in Section \ref{ch2:eoveri}, deferring the description of the MCMC method of detecting variability to Chapter \ref{chapter_mcmc}.


\section{The Sloan Digital Sky Survey}
    \label{ch2:sdss}
    SDSS is an imaging and multi-fiber spectroscopic survey program using the 2.5~m optical telescope at Apache Point Observatory in New Mexico.  SDSS began data collection in 2000, and has operated through the present day in the optical and near-infrared with surveys focused on nearby and distant galaxies, supernovae, quasars, exoplanets, and stars in the Milky Way.

    The original Legacy survey from SDSS-I (2000-2005) imaged more than $8000\ \sqdeg$ of the sky in five optical filters, and obtained spectra of galaxies and quasars.  The original spectrograph was a 640-fiber optical spectrograph, operating in the 4000\ \AA\ -- 9000\ \AA\  wavelength range, with a resolution $R\sim2000$ and a pixel scale of \pixres.  Upon completion of the Legacy survey, SDSS had imaged over 2 million objects and obtained spectra for 800,000 galaxies and 100,000 quasars.

    SDSS-II (2005-2008) saw the introduction of two new surveys using the original camera and spectrograph.  The Supernova Survey \citep{frieman08} scanned $300\ \sqdeg$ of the sky, finding thousands of supernovae and variable objects.  The second survey, the Sloan Extension for Galactic Understanding and Exploration (SEGUE; \citealp{yanny09}), targeted 240,000 stars in the Milky Way, creating a map and providing a detailed picture of the age, composition, and distribution of stars in our Galaxy.

    Additional surveys comprise SDSS-III (2011-2014).  Using the original SDSS spectrograph, the SEGUE survey was expanded with SEGUE-2 (C. M. Rockosi et al., in preparation), focusing on the Milky Way Halo and adding 120,000 stars.  The Multi-object APO Radial Velocity Exoplanet Large-area Survey (MARVELS) monitored 11,000 bright stars with the MARVELS spectrograph, looking for the signatures of exoplanets, but had minimal success.  A high-resolution infrared spectrograph was added for the APO Galactic Evolution Experiment (APOGEE; S. R. Majewski et al., in preparation) survey, aimed at 100,000 red giants across the Milky Way.  SDSS-III also contained the Baryon Oscillation Spectroscopic Survey (BOSS; \citealp{dawson13}), designed to measure the expansion rate of the universe through measurements of the spatial distribution of luminous red galaxies.  With the BOSS survey came an upgrade to the SDSS spectrograph with new CCDs and an increase from 640 to 1000 simultaneous fiber pluggings.

    SDSS-IV (2014-2020) continues today with the extension of two SDSS-III surveys; APOGEE-2 and eBOSS continue to survey stars in the Milky Way and baryonic oscillations in the universe. Additionally, the new Mapping Nearby Galaxies at APO (MaNGA) survey will examine the detailed internal structure of 10,000 nearby galaxies with integral field units.

    \subsection{Sub-Exposures}
        \label{ch2:sub-exposures}
        When a cosmic ray strikes a CCD detector, an artifact appears as many pixels become saturated.  For spectra, these affected pixels often lead to incorrectly reported increases in flux at particular wavelengths.  To facilitate the removal of these artifacts from spectra in SDSS, the original data processing pipeline uses the median flux values at each pixel averaged over multiple sub-exposures taken in succession.  This requirement means that all single observations of an astronomical source actually contain several sub-exposures taken over some short period in time.  Figure \ref{fig:2939-54515-194_indiv} of Section \ref{ch3:examples} provides an example, for an F-type dwarf star, of a coadd spectrum with cosmic rays removed, along with the individual sub-exposure spectra used to construct the spectrum.  Sub-exposure spectra, although lower in \snr\ and beset with cosmic rays, add a time dimension to the data that can be used for monitoring spectral variability.

        In the Legacy and SEGUE surveys, typical spectra are composed of three sub-exposures, with typical exposure times of about 15 minutes.  This presents the potential for detecting variability in spectra which is expected to occur at timescales on the order of hours.  Additionally, many areas of the sky were re-observed over the years for calibration and scientific purposes, yielding additional sub-exposures and increasing the range of timescales that can be probed.  With these re-pointings included, sub-exposure counts per object in the sample of \cite{hettinger15} range from 3 to 47, with baselines ranging from 30 minutes to over 9 years (Figure \ref{fig:nExp_and_baseline}).  

        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/nExp_and_baseline_big}
            \caption{Distribution of the number of sub-exposures (top) and the time lags (bottom) for the F-dwarf stars from the \cite{hettinger15} sample.  Metallicity cutoff values are $\feh=-1.43$ and $\feh=-0.66$.}
            \label{fig:nExp_and_baseline}
        \end{figure*}

        Because cosmic-ray removal must be handled by all spectroscopic observations, sub-exposures are an expected product of all multi-fiber programs.  As we enter an era of large spectroscopic datasets, there is an increasing potential for variability studies through data mining of sub-exposure spectra.  Thus, we are motivated to develop the tools and techniques for extracting information from these sub-exposures.  We encourage the designers of future multi-fiber programs to consider the choice in sub-exposure frequency and cadences, in addition to target selection and scheduling, in order to maximize scientific potential.


    \subsection{SEGUE Stellar Parameter Pipeline And Sample Selection}
        \label{ch2:sspp}
        With the introduction of the SEGUE survey in SDSS-II, aimed at investigating the stellar properties and composition of the Milky Way components, there was a concerted effort to build a pipeline for automatically determining stellar parameters of stars from the abundant spectra provided by the SDSS multi-fiber spectrograph.  The SEGUE Stellar Parameter Pipeline (SSPP; \citealp{lee08}) was designed to carry out these automated tasks.  The SSPP provides, using a variety of techniques, fundamental stellar atmospheric parameters such as metallicity \feh, effective temperature \teff, and surface gravity \logg, as well as spectral classification.  A resource such as the SSPP is invaluable for increasing scientific gains obtained from data mining techniques applied to large, multi-fiber spectroscopic surveys.

        The SSPP was used for identifying the sample groups defined in \cite{hettinger15}.  Stars of spectral type F were selected using the (Hammer method) spectral classifications provided by the SSPP.  Unevolved, main sequence stars were used by selecting dwarfs with surface gravities $\logg\geq 3.75$.  The decision to use F-type stars was based on several factors.  The combination of high frequency, and sufficiently bright luminosity, yields a statistically robust sample with decent \snr\ values.  The intrinsic variability in F-type \rvs\ due to surface activity is relatively low and will have minimal impact on measurement uncertainties.  Also, F-type stars have main sequence lifetimes greater than 5 Gyr, allowing us to select unevolved stars from both the younger disk and the older halo.  The \cite{hettinger15} sample was further divided into three metallicity groups, aimed at tracing the Milky Way components, using the stellar parameters provided by the SSPP (Figure \ref{fig:stellar_params}).
        \begin{figure*}[htbp]
            \centering
            \includegraphics[height=8in]{figures/stellar_params_big}
            \caption{Distribution of stellar parameters for the F-dwarf stars from the \cite{hettinger15} sample, including metallicity (top), effective temperature (middle), and surface gravity (bottom).  Metallicity cutoff values are $\feh=-1.43$ and $\feh=-0.66$.}
            \label{fig:stellar_params}
        \end{figure*}
        Figure \ref{fig:feh_logg_scatter} illustrates the \feh\ and \logg\ distribution of F-type stars in the SSPP DR9 dataset.  
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/dr9_feh-logg_scatter.png}
            \caption{Scatter plot showing the distribution of metallicity and surface gravity for F-type stars in the SSPP DR9.  The bimodal distribution of \feh\ traces the Halo and Disk components of the Milky Way.}
            \label{fig:feh_logg_scatter}
        \end{figure*}
        The bimodal \feh\ distribution separates the Halo and Disk components, and the tails at lower values of \logg\ identify the stars that have evolved away from the Main Sequence.


    \subsection{Plate Systematics}
        \label{ch2:plate_systematics}
        The spectra used in \cite{hettinger15} are from the SDSS Legacy, SEGUE-1, and SEGUE-2 surveys, captured using the original SDSS spectrograph.  The original spectrograph operated by plugging 640 optical fibers into holes (corresponding to each target's position on the sky) on one of the many pre-drilled observing plates.  Each spectrum was given an ID in the form of plate-mjd-fiber using the plate ID number, the Modified Julian Date of the observation, and the fiber ID number.

        After measuring \rvs\ for F-type stars with the methods detailed in Section \ref{ch2:radialVelocities}, correlations in \rv\ variations were found for fibers residing on the same plate.  Correlations were typically seen among fibers that were adjacent to each other on the CCD.  Figure \ref{fig:exp_shift_success} illustrates the systematic issues with an example from plate plugging 2085-53379.  This figure shows the measured \rvs\ of the four sub-exposures for each of the eight F-type dwarfs found on this plate.  
        \begin{figure*}[htbp]
            \centering
            \includegraphics[height=8in]{figures/exp_shift_success.eps}
            \caption{\rvs\ for F-dwarf stars located on plate plugging 2085-53379 before and after correcting the plate for systematic sub-exposure offsets.  Red points are sub-exposures that have low \snr.  Fiber IDs are given for each fiber on the right axis.  Sub-exposures in the plate are ordered chronologically.  Corrections to systematic offsets are successful on this plate.}
            \label{fig:exp_shift_success}
        \end{figure*} 
        It is clearly seen that the \rvs\ of the stars decrease systematically from the first to second sub-exposures, decrease furthermore from the second to third sub-exposure, and increase slightly on the last sub-exposure.

        To correct the plate systematics, we adopted a Markov chain Monte Carlo (MCMC) method to estimate the systematic shifts from sub-exposure to sub-exposure (Section \ref{ch3:plate_shift}).  10,264 fiber sub-exposures were examined across 2453 plates.  The \rv\ corrections applied to these sub-exposures are distributed as seen in Figure \ref{fig:exp_shift_hist}, with a standard deviation of $2.2\ \kmps$ and corrections as large as $17\ \kmps$.

        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/exp_shift_histogram.eps}
            \caption{Distribution of 10,264 systematic \rv\ offsets estimated for all plate sub-exposures in the F-dwarf sample.}
            \label{fig:exp_shift_hist}
        \end{figure*}

        Unfortunately, not all plates could be corrected using the MCMC methods, because of multiple correlated subsets of fibers on the same plate.  Using plate 3002-54844 (Figure \ref{fig:exp_shift_fail}) as an example, we can see why simple techniques to correct systematic shifts are not successful.  In this plate, the \rvs\ from the F-dwarf stars that were exposed on one area of the CCD (Panel~a) are correlated with each other, but anti-correlated with the fibers exposed on another area of the CCD (Panel~d).  Because of this, a systematic shift in \rv\ for a single exposure on a plate does not increase the total value of the MCMC likelihood (Equation \ref{eq:plateShiftLikelihood}).  It is shown from the correction column of Figure \ref{fig:exp_shift_fail}, that the systematics were not resolved.  This lead to the inspection and removal of 25 plates (Table \ref{tbl:badPlates}), requiring manual inspection of sub-exposure \rv\ shifts.

        \begin{table}[h]
            \vspace{\baselineskip}
            \centering
            \caption{Suspect Plates in SDSS}
            \begin{tabular}{ccccc}
                \hline
                \hline
                            &              &  Plate-MJD    &             &              \\
                \hline
                0888-52339  &  2252-53565  &  2683-54153  &  2890-54495  &  2940-54508  \\
                1665-52976  &  2252-53613  &  2701-54154  &  2899-54568  &  3111-54800  \\
                2042-53378  &  2393-54156  &  2839-54461  &  2900-54569  &  3166-54830  \\
                2053-53446  &  2670-54115  &  2856-54463  &  2905-54580  &  3187-54821  \\
                2055-53729  &  2682-54401  &  2861-54583  &  2911-54631  &  3207-54850  \\
                \hline
            \end{tabular}
            \caption*{Plates with systematic uncertainties removed from \cite{hettinger15}, listed by plate number and Modified Julian Date.}
            \label{tbl:badPlates}
        \end{table}

        It is unclear at this point where the source of the systematic shifts in radial velocity seen on plates in the SDSS sub-exposures can be traced to.  We strongly encourage individuals who wish to use sub-exposure information from the SDSS spectrograph to be aware of these issues and take them into consideration when analyzing radial velocities.
        \begin{landscape}
            \thispagestyle{empty}
            \centering
            \begin{figure}
                \subfloat[]{\includegraphics[width=2.1in]{figures/exp_shift_fail_A.png}}\hspace{0.1in}
                \subfloat[]{\includegraphics[width=2.1in]{figures/exp_shift_fail_B.png}}\hspace{0.1in}
                \subfloat[]{\includegraphics[width=2.1in]{figures/exp_shift_fail_C.png}}\hspace{0.1in}
                \subfloat[]{\includegraphics[width=2.1in]{figures/exp_shift_fail_D.png}}\hspace{0.1in}
                \caption{Same as Figure \ref{fig:exp_shift_success} for plate plugging 3002-54844.  The left column of each subfigure shows \rvs\ before correcting the plate for systematic offsets, and the right column shows \rvs\ after correcting for offsets.  Corrections are not successful on this plate due to anti-correlated subsets of similarly correlated fibers.}
                \label{fig:exp_shift_fail}
            \end{figure}
        \end{landscape}


\section{Radial Velocities}
    \label{ch2:radialVelocities}
    Radial velocities can be derived from spectra using the Doppler shift by comparing the wavelengths of the observed absorption features of the source $\lambda_o$, to the rest-frame expectations $\lambda_e$.  The velocity $v$, relative to the speed of light is
    \begin{equation}
        \label{eq:doppler}
        \frac{v}{c}=\frac{\lambda_o-\lambda_e}{\lambda_e}.
    \end{equation}
    Where appropriate, this is accomplished through a cross-correlation of the object's spectrum with a theoretical or empirically derived template.  In this section, we discuss the methods developed in this work for determining \rvs\ through template cross-correlation, referencing the implementation of these methods in \cite{hettinger15}.  We address the processes for normalizing spectra, creating a spectral template, and performing the cross-correlation measurements.

    \subsection{Continuum Normalization}
        \label{ch2:contNormalization}
        Cross-correlation requires that spectra be continuum normalized, such that significant deviations from unity are attributed to absorption and emission features, rather than the blackbody continuum.  The process of normalizing a spectrum is as follows.

        A model of the continuum is estimated by generating a modified version of the spectrum, which is a greatly smoothed copy of the spectrum.  The smoothed copy has all outlying pixels cleaned and set equal to the median flux value.  Absorption lines must be masked out prior to modeling the continuum.  The patched regions to be masked are specified explicitly with a set of wavelength limits where the absorption features are expected to appear for the particular astronomical source.  See Table \ref{tbl:maskRegions}, for example, for a list of the major regions that were masked out in the F-dwarf stars.
        \begin{table}[h]
            \vspace{\baselineskip}
            \centering
            \caption{Absorption Features in F-dwarfs}
            \begin{tabular}{ccc}
                \hline
                \hline
                Wavelength Range   & Feature  \\
                \hline
                3600\ \AA\ -- 4000\ \AA   &  Ca H,K and others \\
                4070\ \AA\ -- 4130\ \AA   &  H$\delta$         \\
                4290\ \AA\ -- 4360\ \AA   &  H$\gamma$         \\
                4830\ \AA\ -- 4890\ \AA   &  H$\beta$          \\
                6540\ \AA\ -- 6580\ \AA   &  H$\alpha$         \\
                \hline
            \end{tabular}
            %\caption*{Wavelength regions of significant absorption line features for F-type dwarf stars.}
            \label{tbl:maskRegions}
        \end{table}
        The entirety of each region is replaced with a patch flux value, calculated by taking the median of all flux values in the region where the values are greater than the median value.  In other words, the region is patched with a continuous flux value equal to an average value, ignoring absorption features.  See Figure \ref{fig:spectrum_normalization} for an example of an F-type star with the absorption regions patched out.  Masking absorption features this way, on a relatively simple spectrum such as that from an F-type star, can be done easily, however, spectra with more intense or more complicated features, such as those of an M-dwarf, would require an alternative method for continuum normalization \citep{ness15}.  With the absorption regions masked out, a smoothed version of the spectrum is produced by removing high frequencies in the Fourier Transform using a FFT smoothing algorithm.  Finally, the original spectrum is divided by the smooth continuum to obtain the continuum normalized spectrum.  It is important to perform the normalization process correctly.  Early attempts with poor continuum fitting at the ends of the spectra resulted in asymmetric distributions in measured radial velocities.

        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6.5in]{figures/spectrum_normalization_big.eps}
            \caption{Continuum normalization process applied to an F-type star showing: (a) the raw sub-exposure spectrum, (b) the spectrum with selected absorption features masked out, (c) a smoothed version of the spectrum, and (d) the final continuum-normalized spectrum.}
            \label{fig:spectrum_normalization}
        \end{figure*}    


    \subsection{Spectral Template}
        \label{ch2:template}
        Two common methods for measuring the \rv\ of a source are, identification of line indices, and template cross-correlation.  The former uses an algorithm to identify absorption and emission features in spectra that are expected to be present, and compares the measured centroids of the lines with the expected rest-frame wavelengths of the features to directly measure a Doppler shift.  Methods for locating centroids and correctly identifying lines associated with particular features can be relatively complex.  The latter method, using template cross-correlation \citep{tonry79}, is more simple but requires an accurate template spectrum for each source that you wish to measure \rvs\ from.  In \cite{hettinger15}, all sources are F-type dwarf stars, and can be fit with a single template.  One benefit of performing cross-correlation on a large dataset of similar sources, is that a template can be constructed from the data itself.  A master template (Figure \ref{fig:fDwarf_template}) was constructed using high quality spectra from 7207 F-type dwarf stars from SDSS DR10.  

        First attempts at making \rv\ measurements with finely tuned templates yielded little change in measured variations in \rv.  We created several templates composed of sources with similar stellar parameters, allowing \feh, \logg, and \teff\ to vary.  The standard-deviation of individual radial velocities within stars remained virtually the same across the entire \feh\ range, regardless of the choice in template.  Similar behavior was observed for \rv\ variations as a function of \logg\ and \teff.  For simplicity, we used a single template composed of spectra from stars of varying stellar parameters, using all available science primary fibers with a co-add $\snr > 50$.

        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/fDwarf_template_spectrum_big.eps}
            \caption{Full continuum-normalized F-type dwarf template spectrum (top) with a detailed view of the blue end of the spectrum (bottom).  Prominent spectral features are annotated.}
            \label{fig:fDwarf_template}
        \end{figure*}

        Steps for creating a template spectrum are as follows.  All input co-add spectra are continuum-normalized (Section \ref{ch2:contNormalization}).  Next, the co-add spectra are de-shifted into the rest-frame using the ``zBest'' redshift values assigned to the co-adds in the SDSS pipeline.  It should be noted here that while the SDSS pipeline provides \rv\ estimates for the co-add spectrum of each fiber, these were obtained from an average of the individual sub-exposures, thereby averaging over the changes in \rv\ between sub-exposures -- the changes that we are seeking to measure.  Once all of the spectra are shifted to rest-frame wavelengths, each spectrum is fit by a third-order B-spline and resampled to a common wavelength solution.  Finally, all co-add spectra are averaged by taking the mean flux at each wavelength in the resampling.  Figure \ref{fig:templateCreation} illustrates the template creation process for a small set of input co-adds.

        \begin{figure*}[htbp]
            \centering
            \includegraphics[height=8in]{figures/ex_template_creation.png}
            \caption{Example of the template creation process using 7 spectra. From top to bottom: blue end and full spectrum of the input co-add stellar spectra, blue end and full spectrum of the normalized, rest-frame input spectra, blue end and full spectrum of the input spectra resampled to a common wavelength solution, blue end and full spectrum of the template (averaged flux of resampled input spectra).}
            \label{fig:templateCreation}
        \end{figure*}


    \subsection{Cross-Correlations}
        \label{ch2:cross_correlations}
        Radial velocities obtained from the Doppler shift are measured by finding the wavelength shift, in pixels, of the source which maximizes the cross-correlation coefficient of the source and template.  This process is detailed here.  For every star in the sample, each sub-exposure spectrum is normalized (Section \ref{ch2:contNormalization}) and cleaned of cosmic rays and other suspect pixels.  Next, the template spectrum is resampled so that the source and template spectra share the same wavelength value at each pixel.  The correlation coefficient per pixel,
        \begin{equation}
            C = \frac{1}{N} \sum_{i=1}^N y_{1,i}\times y_{2,i}
        \end{equation}
        is calculated using the cleaned source spectrum and the template spectrum, repeated with integer pixel shifts of the source spectrum from $-20$ to $+20$ pixels.  This process produces a cross-correlation function (CCF) that has a maximum value at the best estimate for the Doppler shift of the source.  An example CCF can be seen in Figure \ref{fig:xcorr_function}.

        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/xcorr_function_big.eps}
            \caption{Cross-correlation function (per pixel) for a single sub-exposure spectrum of an F-type star.  Correlation values are calculated at integer pixel lags, and a spline interpolation of the function is fit to these values.  The function peaks at $-3.275$ pixels, or $-229\ \kmps$.}
            \label{fig:xcorr_function}
        \end{figure*}

        The value obtained for the pixel lag can be converted to a Doppler shift in \kmps\ with a conversion that is dependent on the resolution of the spectrum.  For SDSS spectra, this conversion is \pixres.  To obtain sub-pixel precision, a smooth B-spline interpolation is performed on the CCF, yielding a better estimate for the Doppler shift.


\section{Empirical Uncertainties}
    \label{ch2:errors}
    Uncertainties in cross-correlation lag measurements must be estimated empirically or through some Monte Carlo method. For an example of a Monte Carlo method, see \cite{peterson98}, where cross-correlations are performed over many iterations, taking random fluctuations in pixel flux and selecting a random sub-set of pixels at each iteration.  \cite{hettinger15} employed an empirical method, looking at the spread in \rv\ measurements for spectra of similar quality.  From these comparisons, we derived a function that assigns uncertainties based on the \snr\ and \feh\ of each sub-exposure.  Measurement uncertainties are expected to increase for stars with lower metallicity, as these stars have less pronounced absorption features, thereby reducing the signal of, and broadening the peak of, the CCF.  The following describes the steps taken to derive the empirical uncertainties.

    To begin, the mean \rv\ for a star is subtracted from all sub-exposure measurements of the star, resulting in new \rvs\ with a mean value of $0.0\ \kmps$.  Similar measurements can now be compared for all sub-exposures from all stars by identifying spectra with similar \feh\ and \snr\ values;  initial tests showed no significant correlation between measurement uncertainty and other stellar parameters such as \teff\ and \logg.  Once all measurements are separated into sets of like \feh\ and \snr, an empirical estimate for the measurement uncertainty for each \feh-\snr\ set is calculated using the median absolute deviation, or \mad\ \citep{leys13}.  The \mad\ value is the median deviation of all values from the median value, 
    \begin{equation}
        \label{eq:MAD}
        \mad = \median(\left|\rv_i - \median(\rv)\right|),
    \end{equation}
    and is related to the empirical estimate of the measurement uncertainty by
    \begin{equation}
        \label{eq:sigma}
        \sigma = 1.4826\mad.
    \end{equation}
    One example of an empirical uncertainty estimate is shown in Figure \ref{fig:MAD}.  All sub-exposures used in this \feh-\snr\ set have $\feh=-1.75\pm 0.25$ and $\snr=30\pm 2.5$.  This process is repeated for all \feh-\snr\ sets that have at least $800$ measurements.

    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/FeH-175_SNR30_MAD.eps}
        \caption{Distribution of radial velocities (minus systemic velocity) for sub-exposures with $\feh=-1.75\pm 0.25$ and $\snr=30\pm 2.5$.  The distribution has a median absolute deviation of $\mad=3.04\ \kmps$, indicating measurement uncertainties of $\sigma=4.51\ \kmps$.}
        \label{fig:MAD}
    \end{figure*}

    Interpolation of the values obtained for the empirical uncertainty estimates of all \feh-\snr\ sets yields a functional form describing the measurement uncertainty with respect to \feh\ and \snr.  First, a 1-dimensional solution is fit to all \feh-\snr\ sets sharing the same \feh\ value using the form 
    \begin{equation}
        \label{eq:stepA}
        \sigma_{\feh}\ (\snr) = \frac{m_{\feh}}{\snr} + b,
    \end{equation}
    where the uncertainty $\sigma_{\feh}$, is inversely proportional to \snr\ with constants $m_{\feh}$ and $b$.  This is performed independently for each of the nine \feh\ values used in the sets.  The median value $b=1.235$ is adopted and used to refit all solutions,
    \begin{equation}
        \label{eq:stepB}
        \sigma_{\feh}\ (\snr) = \frac{m_{\feh}}{\snr} + 1.23.
    \end{equation}
    The results from this are shown in Figure \ref{fig:errors}(b).  A linear relationship between \feh\ and the constant $m$ is described by
    \begin{equation}
        \label{eq:stepC_linear}
        m\ (\feh) = -26.51\feh  + 50.52,
    \end{equation}
    and is illustrated in Figure \ref{fig:linearFit}.  Altogether, the relationship between the empirical estimate of the measurement uncertainty and the sub-exposure \feh\ and \snr\ takes the form
    \begin{equation}
        \label{eq:stepC}
        \sigma\ (\feh,\snr) = \frac{(-26.51\feh  + 50.52)}{\snr} + 1.23.
    \end{equation}

    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=5.5in]{figures/empirical_errors_big.eps}
        \caption{Empirical uncertainties for cross-correlation measurements in F-type dwarf stars showing uncertainties (a) as a function of \snr\ independently determined for each \feh, (b) as a function of \snr\ with a common constant offset, and (c) as a function of \snr\ and \feh\ as in Equation \ref{eq:stepC}.}
        \label{fig:errors}
    \end{figure*}

    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/step_C_linear_fit.eps}
        \caption{Values of coefficient $m$ from Equation \ref{eq:stepB} as a function of \feh.}
        \label{fig:linearFit}
    \end{figure*}

    Figure \ref{fig:errors}(c) illustrates the final form of the empirical uncertainties with values plotted in lines of constant \feh.  \cite{hettinger15} adopted Equation \ref{eq:stepC} to assign uncertainty values to every sub-exposure measurement in the sample.  The distribution of the assigned uncertainties is shown in Figure \ref{fig:uncertainty_hist}.

    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/uncertainty_hist}
        \caption{Distribution of empirically assigned measurement uncertainties for the F-dwarf stars from the \cite{hettinger15} sample.  Metallicity cutoff values are $\feh=-1.43$ and $\feh=-0.66$.}
        \label{fig:uncertainty_hist}
    \end{figure*}
    

\section{$e/i$ Variability}
    \label{ch2:eoveri}
    After sub-exposure \rvs\ are determined and the uncertainties are well characterized, a metric must be chosen to describe the variability of the source.  One such metric, described by \cite{geller08} and \cite{milliman14}, detects variability in \rv\ measurements by comparing the \rv\ deviations (external variation, $e$) to the typical measurement uncertainty (internal variation, $i$), or \eoi.  This way, stars with differing \snr, and therefore different measurement uncertainties, are weighted appropriately, preventing stars with large \rv\ uncertainties from being interpreted as true variables.  The \eoi\ metric is defined as the ratio of the standard deviation of the star's \rvs\ to the mean of the measurement uncertainties.
    %\begin{equation}
    %    \label{eq:eoi}
    %    \left(\frac{e}{i}\right)^2 = \frac{N^2}{N-1}  \frac{\sum_i^N \left(\mathrm{RV}_i-\meanRV\right)^2  \sigma_i^{-2}}  {\sum_i^N\sigma_i^2 \sum_i^N\sigma_i^{-2}},
    %\end{equation}
    %where \meanRV\ is the mean \rv\ of a star, weighted by the measurement uncertainties $\sigma_i$:
    %\begin{equation}
    %    \label{eq:meanRV}
    %    \meanRV = \frac{\sum_i^N\rv_i\sigma_i^{-2}} {\sum_i^N  \sigma_i^{-2}}.
    %\end{equation}
    Values of \eoi\ significantly larger than $1.0$ indicate an increasing likelihood that a star's variability is not due to measurement uncertainties, but rather to the intrinsic changes in the star's \rv.

    One disadvantage of the \eoi\ method, however, is that it does not use a physical model to describe the system.  For instance, \rv\ variability of a star in the presence of a companion body is expected to show a periodic behavior.  This lack of specificity is what led us to finally adopt the MCMC methods detailed in Section \ref{ch3:rv_fitting}.  It is reassuring though, that examination of the \eoi\ values of the stars in the \cite{hettinger15} sample (Figure \ref{fig:eoveri_hist}) yields a conclusion consistent with that gathered from the MCMC method, with both methods indicating a higher fraction of short-period binaries in the metal-rich component of the Milky Way.

    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/eoveri_hist}
        \caption{Distribution of \eoi, the ratio of the standard deviation of \rvs\ to the typical measurement uncertainty values, for the F-dwarf stars from the \cite{hettinger15} sample.  Metallicity cutoff values are $\feh=-1.43$ and $\feh=-0.66$.}
        \label{fig:eoveri_hist}
    \end{figure*}


\section{Discussion}
    \label{ch2:discussion}
    Time resolved-spectroscopy is not limited to radial velocity measurements.  The processes discussed in this chapter can be modified to look at changes, for example, in the emission and absorption features of stars such as those due to surface activity of cool stars or pulsation of RR Lyrae stars.  Future developments in telescopes and instruments may even lead to useful studies of short-timescale variability of active galactic nuclei. We recommend that future observing programs consider the sub-exposure properties of multi-fiber spectrographs, as they add scientific value in regards to the time-domain dimension of the data.  With careful planning of the exposure times, counts, and frequencies adopted in target selection, the scientific return obtained from survey programs can be maximized.