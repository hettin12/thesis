\section{Introduction}
    \label{ch5:intro}
    As mentioned in Section \ref{ch1:discussion}, the number of systems that will experience a CE phase depends on the details of the separation distribution and the fraction of stars that have short-period companions.  Understanding binary fractions and separation distributions will impact the predicted merger rates and supernova rates \citep{badenes12}.  These properties also shed light onto the details of the star formation process and binary formation process, and speak to the dynamical history of stellar associations and clusters.

    In \cite{hettinger15}, we investigated the short-period binary fraction of F-type dwarf stars in the Milky Way halo and disk.  In this chapter, we extend the work to include an investigation of the separation distribution.  An MCMC sampler has been adopted in conjunction with population-wide Monte Carlo simulations, in order to constrain the separation distributions that are consistent with the data.  We will look at the methodology in Section \ref{ch5:montecarlo}, and conclude with a discussion of the results and limitations in Section \ref{ch5:discussion}.


\section{MCMC and Population-Wide Monte Carlo}
    \label{ch5:montecarlo}
    In our approach to investigate the separation distribution of binaries in the F-dwarf sample, we combined a MCMC sampler with population-wide Monte Carlo simulations.  We assume that the binary separations, in our range of sensitivity ($0.01\ \au < a < 0.16\ \au$), are distributed like a power law with an index parameter $\alpha$,
    \begin{equation}
        f(a) \propto a^\alpha\ .
    \end{equation}
    A power law distribution should be decent approximation to the often-used log-normal distribution, for the narrow range of separations to which we are sensitive.  

    In our MCMC, we have two free parameters describing the population of stars, the separation distribution index $\alpha$, and the fraction of systems that have close-separation binary companions $\fbin$.  We include \fbin, as there is a degeneracy in $\alpha$ and $\fbin$, affecting the distribution of \rv\ variations among the stars in the population.  Figures \ref{fig:alphaSensitivity} and \ref{fig:fbinSensitivity} illustrate how changes in either $\alpha$ or $\fbin$ affect the distribution of \eoi\ values for a population of stars.
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/alpha-sensitivity_240x_60bins.png}
        \caption{Distribution of \eoi\ values for a simulated population based on observations and uncertainties in the metal-rich population of \cite{hettinger15}.  Models illustrate variations in the \eoi\ distribution from changes in separation distribution power law index $\alpha$, while keeping fixed the short-period binary fraction $\fbin=0.03$ and the mass ratio distribution power law index $\beta=0.0$.}
        \label{fig:alphaSensitivity}
    \end{figure*}
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/fbin-sensitivity_240x_60bins.png}
        \caption{Distribution of \eoi\ values as in Figure \ref{fig:alphaSensitivity}, with a varying $\fbin$, and fixed $\alpha=-1.0$ and $\beta=0.0$.}
        \label{fig:fbinSensitivity}
    \end{figure*}

    The distribution of mass ratios in a population also has significant implications on the star formation and binary formation processes, and the expected rates for various post-CE systems.  Unfortunately, changes in mass ratios do not have a strong impact on the observed \rv\ variations (see Figure \ref{fig:betaSensitivity}).  
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/beta-sensitivity_240x_60bins.png}
        \caption{Distribution of \eoi\ values as in Figure \ref{fig:alphaSensitivity}, with a varying $\beta$, and fixed $\alpha=-1.0$ and $\fbin=0.03$.}
        \label{fig:betaSensitivity}
    \end{figure*}
    The statistical approach for estimating multiplicity properties from sparsely sampled \rvs\ is limited in this regard.  Determining mass ratio distributions will likely be limited, for some time, to targeted campaigns consisting of detailed spectroscopic and eclipsing observations.

    Since the parameters in our MCMC sampler define a population of stars, the entire population must be simulated at every step in the chain and compared with the data.  This required us to adopt a single, easily calculated metric, to be used for each star in the population.  The \eoi\ metric (Section \ref{ch2:eoveri}) provides a measurement of the variability of a star's \rv, and will change in magnitude depending on the system's binary separation, orbital orientation, and exposure coverage.

    To simulate an \eoi\ distribution for the population, we run a population-wide Monte Carlo simulation at each step in the MCMC chain.  This is accomplished by simulating \rv\ measurements for every stellar system in the population 60 times, calculating \eoi\ for each system, and normalizing the \eoi\ distribution so it can be compared with the data.  

    For individual stellar simulations, a binary companion is either absent or present, based on the value of the population-wide \fbin\ parameter at that step in the MCMC chain.  Next a separation is chosen randomly from a power law distribution set by the $\alpha$ parameter at that step in the MCMC chain, with the separation confined within the limits of our sensitivity ($0.01 - 0.16\ \au$).  Primary masses are drawn from an IMF \citep{kroupa01}, and secondary masses are drawn from a flat distribution from 10\% to 100\% of the primary mass.  \rv\ measurements are simulated using the same observing times that the star was originally observed at.  Finally, measurement uncertainties are folded in, using the empirically estimated \rv\ uncertainties previously derived for that particular star (Section \ref{ch2:errors}).

    To improve the statistics of the model at high \eoi\ values, the population is simulated 60 times at every step in the MCMC chain.  The result in a relatively smooth model, representing a typical \eoi\ distribution for the population of stars, given a specific pair of values for $\alpha$ and $\fbin$.  The likelihood function in the MCMC compares the histogram of \eoi\ values for the population, calculated from the data, with the histogram of \eoi\ values from the model using Poisson probabilities at each bin.  Thus, at each step in the MCMC, one possible population of stars is simulated robustly to produce a \eoi\ curve, and that curve is compared to the histogram of true \eoi\ values previously calculated from the dataset.  This comparison allows the MCMC to accept and reject future proposals for $\alpha$ and \fbin.

    The MCMC sampler was run on all three metallicity groups defined in \cite{hettinger15}.  In Figures \ref{fig:hyperparam_samples_poor}, \ref{fig:hyperparam_samples_inter}, and \ref{fig:hyperparam_samples_rich}, we show the \eoi\ distributions of the metal-poor, -intermediate, and -rich groups.  Overplotted in each figure is a random selection of models, constructed from $\alpha$ and $\fbin$ parameters drawn from the posterior.  It should be noted that the MCMC likelihood function only compared bin heights for bins with $\eoi > 3.0$.  The reasoning is that our empirically determined estimates for \rv\ uncertainties had limited accuracy.  This results in some deviations in the calculated \eoi\ values.  Since the majority of stars have low \eoi\ values, small deviations in the assigned uncertainties lead to large deviations in the bin heights.  Thus the likelihood function would be dominated by differences in the low-\eoi\ values, and would have reduced sensitivity to the high-\eoi\ binary systems.
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=5in]{figures/metal-poor_popEmcee_samples.png}
        \caption{\eoi\ distribution for the metal-poor group.  Model \eoi\ distributions are shown using values of $\alpha$ and $\fbin$ randomly sampled from the MCMC posterior.  The dashed line represents the cutoff, below which bin heights were not used in the likelihood function.}
        \label{fig:hyperparam_samples_poor}
    \end{figure*}
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=5in]{figures/metal-inter_popEmcee_samples.png}
        \caption{Same distribution as in Figure \ref{fig:hyperparam_samples_poor}, for the metal-intermediate group.}
        \label{fig:hyperparam_samples_inter}
    \end{figure*}
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=5in]{figures/metal-rich_popEmcee_samples.png}
        \caption{Same distribution as in Figure \ref{fig:hyperparam_samples_poor}, for the metal-rich group.}
        \label{fig:hyperparam_samples_rich}
    \end{figure*}
    The posterior distributions for $\alpha$ and \fbin\ are depicted in Figures \ref{fig:hyperparam_posterior_poor}, \ref{fig:hyperparam_posterior_inter}, and \ref{fig:hyperparam_posterior_rich}.  The results from the MCMC run are discussed in the following section.
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=3in]{figures/metal-poor_popEmcee_triangle.png}
        \caption{Posterior distribution for the MCMC run of the metal-poor group, with parameters for the short-period binary fraction \fbin, and separation distribution $\alpha$.}
        \label{fig:hyperparam_posterior_poor}
    \end{figure*}
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=3in]{figures/metal-inter_popEmcee_triangle.png}
        \caption{Posterior distribution for the MCMC run of the metal-intermediate group, with parameters for the short-period binary fraction \fbin, and separation distribution $\alpha$.}
        \label{fig:hyperparam_posterior_inter}
    \end{figure*}
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=3in]{figures/metal-rich_popEmcee_triangle.png}
        \caption{Posterior distribution for the MCMC run of the metal-rich group, with parameters for the short-period binary fraction \fbin, and separation distribution $\alpha$.}
        \label{fig:hyperparam_posterior_rich}
    \end{figure*}


\section{Discussion}
    \label{ch5:discussion}
    % Results from MCMC
    Unfortunately, there is little additional information to be drawn from the posterior of these MCMC runs.  The short-separation binary fractions allowed by the data (2\% -- 8\%) are consistent with the values obtained in \cite{hettinger15} through the hierarchical MCMC method.  

    These population-wide MCMC runs favored larger values of $\alpha$, up to the prior limit imposed at $\alpha=6$.  Previous MCMC runs with a much larger range in allowed $\alpha$ values held that large values ($\alpha > 20$) achieve the same posterior density as modest values ($\alpha\sim6$; Figure \ref{fig:hyperparam_posterior_rich_extended}).
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=3in]{figures/metal-rich_popEmcee_triangle_expanded.png}
        \caption{Posterior distribution for the MCMC run of the metal-rich group, with an extended prior limit in $\alpha$.}
        \label{fig:hyperparam_posterior_rich_extended}
    \end{figure*}
    This is likely due to the maximum separation ($a=0.16\ \au$) that was imposed from our definition of \fbin.  For any \fbin, arbitrarily large values of $\alpha$ simply concentrate binaries at $a\simeq0.16\ \au$.  At larger $\alpha$, all binary fractions within a reasonable range produce a similar \eoi\ distribution.

    % alpha fbin correlation
    As expected, at lower values of $\alpha$, we do see a correlation with \fbin, where a smaller value of \fbin\ is required for separation distributions favoring shorter periods.  With higher quality data, this region of parameter space would be more tightly constrained.

    % vs. FeH
    Across the \feh\ groups, the values of $\alpha$ and \fbin\ are broadly consistent with one another.  An exception is, for the metal-intermediate group, there is increased density in the posterior at lower values of \fbin.  This is likely due to the lack of objects with $8<\eoi<12$ in this \feh\ group.  Also, the MCMC seemed to ignore the two highest \eoi\ objects.

    % Limitations of our work
    The detection efficiency and period sensitivity of our method are determined by the survey parameters.  The low spectral resolution of the SEGUE data requires stringent quality cuts to ensure that sub-pixel \rv\ shifts can be detected at each epoch.  The total number of stars in \cite{hettinger15} which survived quality cuts amounts to only $\sim15,000$ among the $\sim250,000$ SEGUE targets classified as F-type.  Typical \rv\ errors in SEGUE spectra are $\sim4\ \kmps$, limiting the longest periods and separations that can probed.

    The APOGEE survey has a higher resolution ($R \sim20,000$) and smaller \rv\ uncertainties ($\sim0.5\ \kmps$).  Applying our techniques to this dataset will result in a much higher detection efficiency, and will require less stringent quality cuts.  Preliminary work with APOGEE data yields a promising distribution of maximum \rv\ variations, with thousands of likely binaries with \logp\ (d) as high as 3 (Figure \ref{fig:apogee_prelim}).
    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=4in]{figures/apogee_prelim.png}
        \caption{Preliminary distribution of maximum \rv\ variation for DR12 APOGEE targets.}
        \label{fig:apogee_prelim}
    \end{figure*}

    % Future work and APOGEE
    We would like to see statistical time-resolved spectroscopy techniques continue to be employed in the future.  Work on the APOGEE set of stars has begun already, and an extension to other spectral classes within SEGUE can be accomplished with little modification to the original pipeline.  Further down the road, we expect similar techniques described in this dissertation will be applied to other current and upcoming multi-fiber spectroscopic missions such as LAMOST, DESI, and PFS, which are expected to collect millions of stellar spectra in the next few years.