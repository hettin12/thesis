\section{Introduction}
    \label{ch3:intro}
    Many problems in astronomy and astrophysics are solved using complicated, costly models with large numbers of free parameters, often combined with low signal-to-noise observations.  Because of this, there has been an increased adoption of probabilistic analysis, such as Bayesian inference.  Additionally, with the increase in power and efficiency of modern day computers, numerical methods like Markov chain Monte Carlo (MCMC) methods are being used to solve problems that would have been previously unsolvable.  \cite{hogg10} include more arguments for why one would want to adopt numerical data analysis techniques, and additionally provide instructions for using MCMC techniques for fitting models to data.

    In this chapter, we describe how MCMC methods can be used in an astrophysical context, referring to their use in \cite{hettinger15} as an example.  We begin with a brief introduction to Bayesian inference in Section \ref{ch3:mcmc}.  In Section \ref{ch3:emcee}, we introduce the \emcee\ Python package and explain how an ensemble MCMC algorithm is used to find expectation values of parameters of interest from a marginalized sampling of the posterior.  In Section \ref{ch3:plate_shift}, we illustrate the use of the \emcee\ package in identifying sub-exposure systematics in SDSS spectra.  Section \ref{ch3:rv_fitting} offers a description of the process for determining stellar variability from the \rv\ curve of a star using a hierarchical MCMC method.


\section{Bayesian Inference and MCMC}
    \label{ch3:mcmc}
    We begin with a brief introduction to Bayesian inference.  Bayesian inference derives a posterior probability that observations $D$, can be described by some model with a vector of parameters $\Theta$, as it relates to the likelihood function of the model and a prior probability of the model parameters.  Specifically, Bayesian inference computes the posterior probability from Bayes' theorem:
    \begin{equation}
        p(\Theta|D) = \frac{1}{Z}p(D|\Theta)p(\Theta).
    \end{equation}
    Here, $p(\Theta|D)$ is the posterior probability, which specifies the probability that a set of model parameters can be inferred from the data, subject to the defined likelihood and prior.  In other words, it describes the probability that a hypothesis is correct, after the observations are collected.  The likelihood $p(D|\Theta)$, describes the compatibility of the observations with the model parameters.  The prior probability $p(\Theta)$, specifies the previous estimate, if any, that the model parameters are correct, before any knowledge of the data is taken into account.  The model evidence $Z$, is a normalization factor which remains constant for all choices of parameter values, and can therefore be ignored for our purposes.

    A posterior probability density function (PDF) gives the posterior probability for all of possible model parameters, having maximum values for sets of parameters which are most likely to yield the observations.  Computing the PDF is often difficult due to complex likelihood functions.  MCMC methods can be adopted to approximate the PDF, numerically, by sampling from the posterior with a class of algorithms.  With the sampling from the PDF, MCMC methods easily allow marginalization over nuisance parameters (parameters that are required by the model, but are of less interest) to retrieve the probabilities and expectations for values of any parameter of interest to the problem.

    Markov chain Monte Carlo methods are a class of algorithms designed to sample from a probability distribution.  They do so by constructing a Markov chain that allows a walker to move between states in parameter space with some transition probability.  At each step in the chain, some set of parameters are compared with the data through a likelihood function.  The transition probabilities between steps are related to the relative likelihood values of each state.  MCMC chains have the property that in the limit that the chain takes an infinite number of steps, the density of states sampled represents the PDF for the model.  When the chain has reached the number of steps where this is approximately true, the chain is said to have converged.  Steps can finally be selected randomly, providing a sample of the PDF.  In the next section, we show how an MCMC algorithm can be used to find the best choice of model parameters from a set of observations.


\section{\emcee: The MCMC Hammer}
    \label{ch3:emcee}

    \subsection{An Affine-Invariant Ensemble Sampler}
        A commonly used MCMC method is the Metropolis-Hastings (MH) method.  The MH method proposes chain steps based on some distribution (such as a multivariate Gaussian) centered on the current chain position.  This requires a number of tuning parameters which scales as $N(N+1)/2$, where $N$ is the dimensionality of the model.  Configuring many tuning parameters is costly and requires many burn-in steps, especially for highly anisotropic densities.  The Python package \emcee\ \citep{foreman13} addresses this issue by implementing the affine-invariant ensemble sampling algorithm proposed by \cite{goodman10}.  \emcee\ uses an ensemble of parallel chain walkers that take steps in series.  Before each walker takes a step, a step proposal is drawn using a stretch move.  That is, a random walker is selected from the remaining ensemble, and a step is proposed along the vector connecting the two walkers.  This process is affine invariant, meaning the algorithm performs equally well under all linear transformations.  The benefit of using the stretch move algorithm is that only two tuning parameters are required, regardless of dimensionality of the model.  This allows an ensemble of chain walkers to explore anisotropic densities very efficiently.  Additionally, \emcee\ implements multi-threading by running chain walkers as separate threads, greatly increasing CPU efficiency when the code is run on a multi-core machine.  Because of the efficiency with anisotropic densities and the ease of parallelization, \cite{hettinger15} uses \emcee\ for testing binary models on the F-dwarf \rv\ curves.

        %

    \subsection{Using \emcee}
        As an introduction to the \emcee\ package and, more generally, the use of MCMC methods for inference, we present a simple example problem.  For more information on using \emcee, please consult \cite{foreman13}.  In this example problem, we have simulated 30 observations ($X_i$, $Y_i$) from the relationship 
        \begin{equation}
            Y_i = 0.8X_i+0.3\ ,
        \end{equation}
        with simulated measurement uncertainties in $Y$ folded in by drawing from a normal distribution with $\sigma_Y=0.1$ (Figure \ref{fig:mcmc_ex_data}).  
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/mcmc_example_data.eps}
            \caption{Mock observations with $X$ values drawn randomly from the range [0,1] and $Y$ values drawn from $Y_i = 0.8X_i+0.3$.  Simulated measurement uncertainties have been added with values drawn from a normal distribution with $\mu=0.0$, $\sigma=0.1$.}
            \label{fig:mcmc_ex_data}
        \end{figure*}
        We wish to use Bayesian inference and the \emcee\ package to determine the best values for the parameters of some hypothetical model.  

        We begin by suggesting a hypothetical model
        \begin{equation}
            y(x) = \mathrm{m}x + \mathrm{b}\ ,
        \end{equation}
        for which we are trying to determine the best values of $m$ and $b$.  The MCMC ensemble sampler is created in \emcee\ by specifying the number of chain walkers (say $100$), the dimensionality of the model (in our case, 2), and a function used to calculate the posterior probability at each step.  For this problem we have defined the posterior probability to be
        \begin{equation}
            p(\Theta|D) = p(\Theta)p(D|\Theta)\ ,
        \end{equation}
        with a likelihood
        \begin{equation}
            p(D|\Theta) = \prod\limits_{i=1}^{N}  \frac{1}{\sigma_Y\sqrt{2\pi}}\exp\left[-\frac{\left(Y_i-y(X_i)\right)^2}{2\sigma_Y^2}\right]\ ,
        \end{equation}
        and a prior
        \begin{equation}
            p(\Theta) = \left\{  {1,\ \ \ \  \atop 0,\ \ \ \ }  {(-10<m<10)\ \mathrm{and}\ (-10<b<10) \atop \mathrm{otherwise}}  \right.\ .
        \end{equation}

        We've adopted a uniform prior for both $m$ and $b$.  For most simple problems, an uninformative prior is a good choice, assuming most of the probability density lies within the limits of the prior.  Since the prior is multiplied by the likelihood, if no information about the prior is given, the posterior will be equal to the likelihood, and therefore the peak in the posterior will be equal to the maximum likelihood.

        After the ensemble sampler is defined, we initialize the starting positions for the $100$ walkers.  A simple choice for starting positions is a random distribution in parameter space defined by the prior limits.  We can now run the ensemble sampler chain for some number of steps $S$.  The total number of steps needed for a robust solution depends on the complexity of the problem and the starting positions of the walkers.  We would like enough steps such that the chains converge, and we want enough steps so as to produce a few independent representative samplings of the posterior.  We can see from the chain history (Figure \ref{fig:mcmc_ex_timeline} and Figure \ref{fig:mcmc_ex_chainSteps}) that all chains converge in about $60$ steps.  Therefore, we want at least 60 steps per chain, while burning and removing the first 60 steps from the final distribution.
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/mcmc_example_timeline.eps}
            \caption{Top: values of $m$ from the MCMC for the first 60 steps taken by all 100 chain walkers.  Convergence is reached by step 60.  Bottom: same for the parameter $b$.}
            \label{fig:mcmc_ex_timeline}
        \end{figure*}
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/mcmc_example_chainSteps_b.eps}
            \caption{Left: parameter-space location of the first 60 steps taken for 7 of 100 chain walkers.  Stars represent the inital position of each chain walker, and subsequent steps are displayed as circles of decreasing radius.  Right: a closer view of the center of the chain step distribution.}
            \label{fig:mcmc_ex_chainSteps}
        \end{figure*}
        Additionally, we may want to thin the sample to reduce autocorrelation.  Because chain walkers often take steps proposed in their vicinity, there is some correlation among adjacent steps.  To mitigate this effect, we will keep every $3$rd step from a walker, and discard the remainder.  Figure \ref{fig:mcmc_ex_samples} shows the solutions for 300 randomly selected ($m$,$b$) values sampled from the MCMC posterior.  Figure \ref{fig:mcmc_ex_triangle} depicts the final posterior distributions for our model parameters.  Sub-figure (a) is the joint PDF of the two variables $m$ and $b$.
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/mcmc_example_samples.eps}
            \caption{Linear solutions for 300 randomly selected ($m$,$b$) pairs sampled from the MCMC posterior distribution.}
            \label{fig:mcmc_ex_samples}
        \end{figure*}
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/mcmc_example_triangle}
            \caption{Posterior probability distribution for: (a) the join distribution of parameters $m$ and $b$, (b) parameter $m$, marginalized over parameter $b$, and (c) parameter $b$, marginalized over parameter $m$.  Blue lines indicate values of $m$ and $b$ used in the creation of the mock data points.}
            \label{fig:mcmc_ex_triangle}
        \end{figure*}
        If we ignore one parameter and take the histogram for the other, we obtain the marginalized PDF for a single variable.  Marginalizing over a nuisance parameter propagates the effects of the uncertainty in the nuisance parameter into the PDF of the marginalized parameter.  The marginalized PDFs for $m$ and $b$ are also depicted Figure \ref{fig:mcmc_ex_triangle}.  

        We finally arrive at our solution.  Using the mean values of the marginalized PDFs, our Bayesian inference yields the values $m=0.77$ with a 68\% credible interval $0.71<m<0.83$, and $b=0.30$ with a 68\% credible interval $0.27<b<0.34$.
        % 100 walkers at 400 steps each.
        % burn 60 steps -> 340 steps per walker
        % thin by 3 steps -> 114 steps per walker
        % for a total of 11400 steps.
        % acor time is about 5 steps from the 114, so there is about 23 independent samplings
        %m =  0.773777342105
        %minus 0.0611773421053
        %plus 0.0604226578947
        %0.68 credible interval  0.7126 < m <  0.8342
        %b =  0.303225491228
        %minus 0.0321254912281
        %plus 0.0329745087719
        %0.68 credible interval  0.2711 < b <  0.3362


\section{Correcting Systematics in SDSS Spectra}
    \label{ch3:plate_shift}
    As discussed in Section \ref{ch2:plate_systematics}, \rv\ measurements of stars from the SDSS were observed to have systematic uncertainties.  Each plate-plugging contains 640 fibers ($M$ of which are F-type stars) that were exposed simultaneously, $N$ times, for a total of $N\times M$ F-type sub-exposures per plugging.  For some plate-pluggings, groups of fibers had positively correlated \rv\ changes.  To quantify the systematic shifts, we employed a MCMC chain with \emcee.  

    Every plate-plugging was checked independently with a single ensemble sampler.  The samplers consisted of $N$ parameters $\epsilon_i$, one for each plate exposure.  These parameters represent the optimal correction that should be applied to the $i$th \rv\ of all $M$ stars on the plate.  The choice in the likelihood function seeks to reduce the scatter of \rvs\ about the stars' mean velocities, with the requirement that the offset applied to exposure $i$ is identical for all stars on the plate.  The log-likelihood\footnote{To prevent issues with numerical precision, MCMC samplers, such as \emcee, use the summation of log-likelihoods rather than the product of likelihoods.} is
    \begin{equation}
        \label{eq:plateShiftLikelihood}
        \ln p(D|\Theta) = -\frac{1}{2}\sum\limits_{j}^{M}\sum\limits_{i}^{N}\left[\ln(\sigma_{j,i}^2) + \frac{\left(\left(Y_{j,i} - \epsilon_i\right) - \left<Y\right>_j \right)^2}{\sigma_{j,i}^2}\right]\ ,
    \end{equation}
    where $Y_{j,i}$ and $\sigma_{j,i}$ are the \rv\ and uncertainty for the $i$th exposure of the $j$th star, and $\left<Y\right>_j$ is the weighted mean velocity of the $j$th star.  We are assuming here that the contribution of scatter due to intrinsic variability in the stars is negligible.  For the prior we use uniform distributions,
    \begin{equation}
        \ln p(\Theta) = \left\{  {0,\ \ \ \  \atop \mathrm{-\infty},\ \ \ \ }  {(-50\ \kmps<\epsilon_i<50\ \kmps)\ \mathrm{for\ all}\ i \atop \mathrm{otherwise}}  \right.\ .
    \end{equation}

    The sampler for each plate uses 200 chain walkers, running 600 steps each, and thinned by a factor of 3.  The first 100 steps were removed as a burn-in.  Figure \ref{fig:mcmc_systematic_triangle} depicts the resulting PDF for an example plate-plugging, 2085-53379.  This plate has 8 F-dwarf stars with 4 sub-exposures each.  The corrections to the radial velocities are illustrated in Figure \ref{fig:exp_shift_success}.

    \begin{figure*}[htbp]
        \centering
        \includegraphics[width=6in]{figures/mcmc_systematics_2085-53379_triangle.png}
        \caption{Two-parameter joint probability distributions for plate-plugging 2085-53379.  All values are in \kmps.  Fully marginalized, single-parameter probability distributions occupy the subplots on the diagonal.  Parameters $dy_i$ ($\epsilon_i$ in the text) represent the corrections to be applied to the $i$th exposure on the plate.}
        \label{fig:mcmc_systematic_triangle}
    \end{figure*}


\section{Modeling Multiplicity With Radial Velocity Curves}
    \label{ch3:rv_fitting}
    The work of \cite{hettinger15} examines the \rv\ curves of all of the F-dwarf stars in the sample to determine the probability that a system has a binary companion detection.  Once again we use \emcee, creating a trans-dimensional, hierarchical MCMC ensemble sampler.  This sampler consists of two models: a single-star model with a sole parameter, and a binary-star model with four parameters.  A hyperparameter $\lambda$, serves as an index for choosing one of the models at each step in the chain.  After marginalizing over all model parameters for both models, the marginalized PDF of $\lambda$ infers the posterior probability that a star's \rv\ curve supports a short-period binary detection.

    The single-star model \smodel, adopts a non-varying \rv\ curve, parameterized by a systemic velocity \vnaught,
    \begin{equation}
        \smodel(t) = \vnaught\ .
    \end{equation}
    In the binary-star model \bmodel, the \rv\ curve is fit by a simple sinusoid defined by four parameters: the log of the semi-amplitude \loga, the log of the period \logp, a phase $\phi$, and a systemic velocity \vnaught,
    \begin{equation}
        \bmodel(t) = A\sin\left(2\pi \frac{t}{P} + \phi\right) + \vnaught\ .
    \end{equation}
    This model assumes circular orbits (eccentricity = 0).  At short periods ($P<12$ days), where our data is most sensitive, circular orbits are nearly ubiquitous \citep{raghavan10}, as orbits become circularized due to tidal forces.

    In addition to the parameters describing the \rv\ behavior of the primary star, we included a number of parameters to mitigate the effects of possible inter-plate systematics, discussed here.  As mentioned in Section \ref{ch2:plate_systematics}, intra-plate systematics were found to have been artificially shifting the \rvs\ of large groups of fibers.  These issues were addressed with MCMC methods, but it remains difficult to find these systematics between plates, because fiber plug positions were often changed between plate observations.  It is reasonable to assume that systematics may be present from plate to plate, so additional parameters $\omega_p$ are used to model possible shifts in \rv\ between plate observations.  This works by allowing all \rvs\ of an individual star taken on plate $p$, to shift systematically relative to the first plate ($p=1$).  By introducing these free parameters, binary models that fit the \rv\ curve based solely on plate-to-plate differences are penalized, while models that show strong \rv\ variations within a plate are less affected.  For most fibers, \rv\ data is drawn from a single plate observation, and therefore do not contain any $\omega_p$ parameters.  Other fibers, which have $P>1$ plate observations, will have a total of $P-1$ plate-shift parameters.

    With the plate-shift parameters and the two \rv\ models defined, the log-likelihood used in the \emcee\ sampler becomes
    \begin{equation}
        \ln p(D|\Theta) = -\frac{1}{2}\sum\limits_{i}^{N}\left[\ln(\sigma_i^2) + \frac{\left(\left(Y_i - \omega_p\right) - \model(t_i) \right)^2}{\sigma_i^2}\right]\ ,
    \end{equation}
    where $\model(t)$ is equivalent to \bmodel$(t)$ whenever $\lambda$ selects the binary model ($\lambda\geq 0.5$), and \smodel$(t)$ otherwise.\footnote{Unfortunately, \emcee\ does not handle binary data types as a model parameter, so we set $\lambda$ as a parameter with uniform prior between 0 and 1, using 0.5 as the dividing point for determining which model to use at any step.}  $Y_i$ is the \rv\ of the star at exposure $i$ with a measurement uncertainty $\sigma_i$.

    The choice in prior distributions for the parameters (Table \ref{tbl:priorLimits}) were considered carefully.  
    \begin{table}[h]
        \vspace{\baselineskip}
        \centering
        \caption{Prior Limits for \cite{hettinger15} MCMC}
        \begin{tabular}{ccc}
            \hline
            \hline
            Parameter   & Lower Limit & Upper Limit \\
            \hline
            $\lambda$          &  0.0   &  1.0     \\
            $\phi$             &  0     &  $2\pi$  \\
            $\omega_i$ (\kmps) &  -20   &  20      \\
            \vnaught\ (\kmps)  &  -600  &  600     \\
            \loga\ (\kmps)     &  0.48  &  2.40    \\  % 3 kmps to 250 kmps
            \logp\ (s)         &  4.0   &  7.0     \\ 
            \hline
        \end{tabular}
        \caption*{Prior limits adopted in \cite{hettinger15} for the parameters in the MCMC ensemble sampler.  Prior probabilities were uniformly distributed within these ranges.  Parameters include the model selector index $\lambda$, orbital phase $\phi$, plate shift parameters $\omega_i$, systemic velocities \vnaught, the log of the semi-amplitude \loga, and the log of the orbital period \logp.}
        \label{tbl:priorLimits}
    \end{table}
    In the regime where the period grows longer or the amplitude decreases in magnitude, the binary-star model gives the same likelihood values as the single-star model.  Therefore, the limits on the prior probabilities should not be arbitrarily large, but should instead be set at a reasonable values that are representative of the sensitivity of the sparsely sample \rvs.  For systems with periods $\logp\ (\second) > 7.0$, the expected \rv\ amplitudes decrease to levels below the measurement uncertainty of our data.  As expected, early MCMC trials found no significant probability for these large periods in the \bmodel\ model.  A lower limit $\logp\ (\second) = 4.0$ is used; this is the orbital period at $a=1\ \rsun$, for which stellar contact between two stars is certain.  For the semi-amplitude, the lower limit on the prior is $3\ \kmps$, comparable to the measurement uncertainties in the \rvs.  The upper limit on the prior is $250\ \kmps$, a value greater than the maximum \rv\ amplitude expected from an F-dwarf system.  Systemic velocities allowed are those less than the escape velocity of the Milky Way.

    The MCMC samplers were executed independently for all stars in the sample using 200 chain walkers, taking a total of $2.4$ million steps.  The chains were burned to convergence, and thinned down to a final size of 600,000 samples per star.  For a description of the findings and a discussion of the results, please refer to Section \ref{sec:multiplicity} and Section \ref{sec:discussion}.  Instead, we will show here the results for select stars, and discuss some features of the posteriors.


    \subsection{Examples}
        \label{ch3:examples}
        The first example star is a binary candidate with the fiber ID 2939-54515-194.  This star is a F-type dwarf star from the \cite{hettinger15} sample, with stellar parameters $\feh=-1.50$ and $\logg=4.3$.  Twelve individual, raw sub-exposures ($\left<\snr\right>=40$), comprise the coadd spectrum (Figure \ref{fig:2939-54515-194_indiv}).  
        \begin{landscape}
            \thispagestyle{empty}
            \centering
            \begin{figure*}[htbp]
                \centering
                \includegraphics[width=9in]{figures/example_stars/2939-54515-194_indiv.eps}
                \caption{Individual sub-exposure spectra (top) used in the production of the coadd spectrum (bottom) for fiber ID 2939-54515-194.}
                \label{fig:2939-54515-194_indiv}
            \end{figure*}
        \end{landscape}
        Sub-exposures were taken on three separate nights within a week for a baseline of 143 hours.  The measured sub-exposure \rvs\ differ by as much as $33.7\ \kmps$, indicating that the star is a likely binary candidate.

        Using the settings from Section \ref{ch3:rv_fitting}, an ensemble of MCMC chain walkers compared the \rvs\ with the binary- and single-star models, ultimately finding the binary model to be the most probable.  Figure \ref{fig:2939-54515-194_chaintraces} reports the parameter values for all chain walkers as a function of step number.  
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/example_stars/2939-54515-194_chaintraces.png}
            \caption{Parameter value progression for all 200 chain walkers in the multiplicity MCMC for fiber ID 2939-54515-194. Chain samples have already been thinned.  Samples earlier than the red dashed line were removed from analysis during the burn-in process.}
            \label{fig:2939-54515-194_chaintraces}
        \end{figure*}
        All parameters converged by step 1000 (3000 before thinning), after which we see virtually no samples with $\lambda<0.5$, indicating a strong probability of the binary model over the single-star model.  We've plotted the radial velocity curve for 2939-54515-194 in Figure \ref{fig:2939-54515-194_samples}, along with potential orbits constructed from 200 randomly drawn samples from the posterior.
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/example_stars/2939-54515-194_samples.png}
            \caption{Orbits constructed from 200 random samples of the MCMC posterior distribution for fiber ID 2939-54515-194.}
            \label{fig:2939-54515-194_samples}
        \end{figure*}
        We see the typical aliasing of the likely orbital periods that one expects to infer from a sparsely sampled \rv\ curve.  The gaps in coverage allow for models with orbits that have shorter periods, often at harmonic frequencies.  Because of this, we may not be able to specify the exact period with confidence, but the MCMC inference does allow us to rule out the single-star model.  This can be seen, for example, when we marginalize over \logp\ (and all other parameters) and look at the the marginalized PDF for the $\lambda$ selection parameter (Figure \ref{fig:2939-54515-194_triangle}, bottom-right).
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/example_stars/2939-54515-194_triangle.png}
            \caption{Posterior probability distributions of parameters in the multiplicity MCMC for fiber ID 2939-54515-194.}
            \label{fig:2939-54515-194_triangle}
        \end{figure*}
        All values of $\lambda$ are $\geq0.5$, indicating a significant preference for \bmodel.  Examining the posterior PDFs in  Figure \ref{fig:2939-54515-194_triangle}, we see other features.  The period aliasing appears again in the marginalized posterior for \logp\ as multiple peaks.  Additionally, a relationship among \vnaught, $\phi$, and \loga\ exists.  To fit the data with models having extreme values of \vnaught, the models must also have larger amplitudes with a phase shift.  Also, these models can only work with longer periods, explaining the relationship seen between \logp\ and \loga.  Larger amplitudes are supported only by longer periods.  With better \rv\ coverage, the region of parameter space that can produce viable orbits to fit the data diminishes, allowing for a more confident estimate of orbital parameters.  Our next example has better coverage and demonstrates a case where the orbital parameters are specified more confidently.

        The next star, fiber ID 2960-54561-375, is an F-type star like 2939-54515-194, but it has a slightly lower surface gravity with $\logg=3.6$.  Because this star's surface gravity is below the conservative definition for dwarf stars defined in \cite{hettinger15}, it was not used in any of the analyses.  However, this star is among the stars with the most constrained values, due to the large variations in \rv, combined with adequate sampling.  Unlike most of the other F-type stars measured with our cross-correlation technique, star 2960-54561-375 has measured velocities that are greater than the dispersion velocity of the SDSS spectra (\pixres), meaning changes in the absorption features have shifts that are greater than a pixel, and are visible by eye.  We highlight this in Figure \ref{fig:2960-54561-375_lines}, where we have plotted a close-up view of three absorption features.  
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/example_stars/2960-54561-375_lines.eps}
            \caption{Changes in redshift with time for fiber ID 2960-54561-375. Absorption lines from normalized sub-exposures are ordered chronologically from top to bottom.  Velocities in each sub-figure are relative to the rest-frame wavelength of Calcium K (left), Calcium H (middle), and H$\alpha$ (right).  Dashed vertical lines represent the mean velocity of the star.}
            \label{fig:2960-54561-375_lines}
        \end{figure*}
        The eight sub-exposures are ordered chronologically from top to bottom, and plotted as a function of redshift velocity.  That is, each pixel's wavelength in the spectrum corresponds to a measured redshift, had that pixel contained the centroid of the absorption feature for that particular line (e.g., Ca K).  To compute this, we convert each pixel wavelength to a velocity from the Doppler shift (Equation \ref{eq:doppler}) using the specific absorption feature as the rest-frame wavelength.  Using the mean value of the star's measured \rvs\ (dashed vertical line) as a guide, shifts in the line centroids over time become apparent.

        Looking at randomly selected orbits from the ensemble sampler (Figure \ref{fig:2960-54561-375_samples}), we see a strong preference for a model with a period of $P=7.94$ days, followed by models with harmonic periods.
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/example_stars/2960-54561-375_samples.png}
            \caption{Orbits constructed from 200 random samples of the MCMC posterior distribution for fiber ID 2960-54561-375.}
            \label{fig:2960-54561-375_samples}
        \end{figure*}
        In the posterior (Figure \ref{fig:2960-54561-375_triangle}), we see the peaks in the marginalized probabilities for all parameters, corresponding to the best model.  We also see strong probability concentrated in a second model with a shorter period at $P=1.14$ days.
        \begin{figure*}[htbp]
            \centering
            \includegraphics[width=6in]{figures/example_stars/2960-54561-375_triangle.png}
            \caption{Posterior probability distributions of parameters in the multiplicity MCMC for fiber ID 2960-54561-375.}
            \label{fig:2960-54561-375_triangle}
        \end{figure*}        
