Readme text for the MSU Ph D Dissertation class file 6/7/2012

A working knowledge of LaTeX is required to prepare your Ph D dissertation using this class file. 

Put the files, msuphddissertation.cls and PhD_Dissertation_Template.tex, in a new folder. Name it in a way that will reflect that it contains your  dissertation. Open the file, PhD_Dissertation_Template.tex, in the editor you use for LaTeX and rename it as your dissertation. Follow the directions in the file to produce your dissertation.
